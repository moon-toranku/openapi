# Toranku project
## OpenAPI
Access to Toranku services is done by the REST services documented on the [OpenAPI](https://gitlab.com/moon-toranku/openapi/-/blob/master/reference/Toranku.yaml)  

You can get more information about the project in the [service](https://gitlab.com/moon-toranku/service)